describe ( "MagentoCategoryTest",() => {
    before(function(){
        cy.visit(`${Cypress.env('url')}`)
    })

it ('Visibility of products in category', () => {
    
    // This test will fail - it will gonna start work if in every category will be subcategories and products
    cy.get('nav ul li:visible').its('length').then( (categoriesLength) => {
        for(var categoryNumber=1; categoryNumber<=categoriesLength;categoryNumber++ ) {
            cy.get(`nav ul li:visible:nth-child(${categoryNumber})`).click()
            cy.get(`.categories-menu > ul:first >li`).its('length').then( (subcategoriesLength) => {
                    const subcategoriesNumber = parseInt((Math.random()*subcategoriesLength)+1)
                    cy.get(`.categories-menu > ul:nth-child(2) > li:nth-child(${subcategoriesNumber}) > a`).click()
                })
            
            cy.get('.product-item-info:visible').should('be.visible')
        }
    })
})
})
