describe ( "MagentoCompareProducts",() => {
    before(function() {
        cy.visit(`${Cypress.env('url')}`)
            cy.server()
            cy.route('GET', `${Cypress.env('url')}/customer/section/load/?sections=compare-products**`).as('CompareProducts')
        })

it ('Can add product to compare list', () => {

    // Choose Gear category (name of this category should never change)
    cy.contains('Gear').click()

    cy.get('#page-title-heading').should('be.visible')

    cy.url().should('include', 'gear')

    cy.get(`.categories-menu > ul:first > li`)
    .its('length')
    .then( categoryLength => {
        const categoriesNumber = parseInt((Math.random()*categoryLength)+1)
        cy.get(`.categories-menu > ul:nth-child(2) > li:nth-child(${categoriesNumber}) > a`).click()
    })

    const AmountOfProduts = parseInt((Math.random()*6)+2)
    for (var i=0; i<=AmountOfProduts; i++) {
        cy.get('.filter-options-content li').should('not.be.visible')
        cy.get('.product-item-info:visible')
        .its('length')
        .then((productsLength) => {
            const productsNumber = parseInt((Math.random()*productsLength))
            cy.get('.product-item-info:visible').eq(`${productsNumber}`).find('[title="Porównaj"]').click({force:true})
        })
    }

    cy.get('.action.compare.primary').should('be.visible').click()
    
    cy.url().should('include', 'product_compare')

    cy.get('.cell.product.info').should('be.visible')

    cy.contains('Porównaj produkty')
})
    })
