describe ( "MagentoRegistrationTest",() => {

it ('Can create account by API', () => {

    cy.visit(`${Cypress.env('url')}`)
   
    cy.get('.panel :nth-child(3) > a').click()

    const myMail = parseInt(Math.random()*1000000) + "testemail@testemail.pl"

    cy.request('POST', `${Cypress.env('url')}/rest/default/V1/customers`,
    {
        "customer": {
        "email": myMail,
        "firstname": "Pawel",
        "lastname": "Tester"
            },

        "password": 'TestPassword123'
    })
    .as('registrationRequest')
    
    cy.get('@registrationRequest').its('body').then((body) => {
            expect(body.email).to.equal(myMail)
            expect(body).to.haveOwnProperty('id')
            expect(body).to.haveOwnProperty('created_at')
         })

    })
})

