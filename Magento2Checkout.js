describe ( "MagentoCheckoutTest",() => {
    before(function(){
        cy.visit(`${Cypress.env('url')}`)
            cy.server()
            cy.route('POST', `${Cypress.env('url')}/rest/pl/V1/guest-carts/**/estimate-shipping-methods`).as('CheckoutDeliveryMethod')
            cy.route('GET',  `${Cypress.env('url')}/customer/section/load/**`).as('CheckoutSecondStep')
        })

it ('Can buy products in Magento shop', () => {
    
    // Choose Women category (name of this category should never change)
    cy.contains('Women').click()

    cy.get('#page-title-heading').should('be.visible')

    cy.url().should('include', 'women')

    cy.get(`.categories-menu > ul:first > li`)
    .its('length')
    .then((categoryLength) => {
        const categoriesNumber = parseInt((Math.random()*categoryLength)+1)
        cy.get(`.categories-menu > ul:nth-child(2) > li:nth-child(${categoriesNumber}) > a`).click()
    })

    cy.get('.product-item-info:visible')
    .its('length')
    .then(productsLength => {
        const productsNumber = parseInt((Math.random()*productsLength))
        cy.get('.product-item-info:visible')
        .eq(`${productsNumber}`)
        .click()
    })

    cy.get('#product-options-wrapper [attribute-code="size"] div [role=option]')
    .its('length')
    .then(function(sizeLength) {
        const sizeNumber = parseInt((Math.random()*sizeLength)+1)
        cy.get(`#product-options-wrapper [attribute-code="size"] div [role=option]:nth-child(${sizeNumber})`)
        .click()
    })

    cy.get('#product-options-wrapper [attribute-code="color"] div [role=option]')
    .its('length')
    .then((colorLength) => {
        const colorNumber = parseInt((Math.random()*colorLength)+1)
        cy.get(`#product-options-wrapper [attribute-code="color"] div [role=option]:nth-child(${colorNumber})`)
        .click()
    })
    cy.get('#product-addtocart-button').click()
    cy.get('[data-ui-id=message-success]').should('be.visible')
    cy.get('.counter.qty:visible').click()
    cy.get('#top-cart-btn-checkout').click()


    const myLastName = parseInt(Math.random()*100000) + "Tester"
    const myPhone = parseInt(Math.random()*1000000)
    const myMail = parseInt(Math.random()*1000000) + "testemail@testemail123.pl"

    //Type all the data
    cy.url().should('eq', `${Cypress.env('url')}/checkout/#shipping`, { timeout: 5000 })
    cy.get('#customer-email').should('be.visible',{timeout:6000} )
    cy.get('#customer-email').type(myMail)
    cy.get('[name="firstname"]').type('Pawel')
    cy.get('[name="lastname"]').type(myLastName)
    cy.get('[name="telephone"]').type(myPhone)
    cy.get('[name="city"]').type('Poznan')
    cy.get('[name="region_id"]').select('kujawsko-pomorskie')
    cy.get('[name="postcode"]').type('61-381')
    cy.get('[name="street[0]"]').type('Osiedle Bajkowe')
    cy.get('[name="street[1]"]').type('921')
    cy.wait('@CheckoutDeliveryMethod')
    cy.wait('@CheckoutDeliveryMethod')
    cy.get('[data-role=opc-continue]').click()
    cy.wait('@CheckoutSecondStep')
    cy.get('.action.primary.checkout').click()
    cy.contains('Dziękujemy za złożenie zamówienia!')

})
    })
