describe ( "MagentoFiltersTest",() => {
    before(function(){
        cy.visit(`${Cypress.env('url')}`)
        })

it ('Can filter products', () => {
    //This test can fail because of wrong working of filters

    // Choose Women category (name of this category should never change)
    cy.contains('Women').click()
    cy.get('#page-title-heading').should('be.visible')
    cy.url().should('include', 'women')

    cy.get(`.categories-menu > ul:first > li`).its('length').then(function(K) {
        const L = parseInt((Math.random()*K)+1)
        cy.get(`.categories-menu > ul:nth-child(2) > li:nth-child(${L}) > a`).click()
    }) 
    cy.get('.filter-options-content li').should('not.be.visible')
    cy.get('.filter-options-item').its('length').then(function(O) {
        const U = parseInt((Math.random()*O)+1)
        cy.get(`.filter-options-item`).eq(`${U}`).click()
        cy.get(' .filter-options-content li span:visible:first').click()
    })
        cy.get('.filter-label').should('be.visible')
})
})
