describe ( "MagentoBasketTest",() => {
    before(function(){
        cy.visit(`${Cypress.env('url')}`)
            cy.server()
            //cy.route('POST', `${Cypress.env('url')}/checkout/cart/add**`).as('AddToBasket')
            cy.route('POST', 'https://demo-magento-2.auroracreation.com/pl/checkout/cart/add/**').as('AddToBasket')
            cy.route('GET',  `${Cypress.env('url')}/customer/section/load/**`).as('AddToMiniBasket')
          //  https://demo-magento-2.auroracreation.com/default/customer/section/load/
            })

it ('Can add right product to basket', () => {
    
    cy.contains('Utwórz konto').click()

    const myLastName = parseInt(Math.random()*100000) + "Tester"
    const myMail = parseInt(Math.random()*1000000) + "testemail@testemail123.pl"
    const myPassword = 'TestPassword123'

    //Type all the data
    cy.url().should('eq', `${Cypress.env('url')}/customer/account/create/`, { timeout: 5000 })

    cy.get('#firstname').type('Pawel')

    cy.get('#lastname').type(myLastName)

    cy.get('#email_address').type(myMail)

    cy.get('#password').type(`${myPassword}`)

    cy.get('#password-confirmation').type(`${myPassword}`)

    cy.get('.action.submit.primary').click()
    
    cy.url().should('eq', `${Cypress.env('url')}/customer/account/`)

    cy.contains('Moje konto')

    cy.get('[data-ui-id="message-success"]').should('be.visible')


    // Choose Women category (name of this category should never change)
    cy.contains('Women').click()

    cy.get('#page-title-heading').should('be.visible')

    cy.url().should('include', 'women')

    cy.get(`.categories-menu > ul:first > li`)
    .its('length')
    .then((categoryLength) => {
        const categoriesNumber = parseInt((Math.random()*categoryLength)+1)
        cy.get(`.categories-menu > ul:nth-child(2) > li:nth-child(${categoriesNumber}) > a`).click()
    })

    cy.get('.product-item-info:visible')
    .its('length')
    .then(productsLength => {
        const productsNumber = parseInt((Math.random()*productsLength))
        cy.get('.product-item-info:visible')
        .eq(`${productsNumber}`)
        .click()
    })

    cy.get('#product-options-wrapper [attribute-code="size"] div [role=option]')
    .its('length')
    .then(function(sizeLength) {
        const sizeNumber = parseInt((Math.random()*sizeLength)+1)
        cy.get(`#product-options-wrapper [attribute-code="size"] div [role=option]:nth-child(${sizeNumber})`)
        .click()
    })

    cy.get('#product-options-wrapper [attribute-code="color"] div [role=option]')
    .its('length')
    .then((colorLength) => {
        const colorNumber = parseInt((Math.random()*colorLength)+1)
        cy.get(`#product-options-wrapper [attribute-code="color"] div [role=option]:nth-child(${colorNumber})`)
        .click()
    })
    
    cy.get('#product-addtocart-button').click()

    cy.wait('@AddToBasket')

    cy.get('[data-ui-id=message-success]').should('be.visible')

    cy.get('.counter.qty:visible').click()

    cy.request('POST', 'https://demo-magento-2.auroracreation.com/rest/default/V1/integration/customer/token', 
    {
        "username": `${myMail}`,
        "password": `${myPassword}`
        }
    ).as('requestToken')
    
    cy.get('@requestToken').its('body').then( (token) => { 
        cy.request({
            url: 'https://demo-magento-2.auroracreation.com/rest/default/V1/carts/mine', 
            auth: {
                bearer : `${token}`
                 }
            }
        ).as('requestBasket')
    })

    cy.get('@requestBasket').its('body.items.0.sku').then ( (skuRequest) => { 
       cy.get('@AddToMiniBasket').its('response.body.cart.items.0.product_sku').then( (skuAddToBasket) => {
          expect (skuRequest).to.equal(skuAddToBasket)
        })
    })
  
        })
    })
