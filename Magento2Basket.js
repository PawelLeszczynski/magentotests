describe ( "MagentoBasket",() => {
    before(function(){
        cy.visit(`${Cypress.env('url')}`)

            cy.server()

            cy.route('POST', `${Cypress.env('url')}/rest/pl/V1/guest-carts/**`).as('CheckoutBasket')
        })

it ('Add products and check sum of prices', () => {

    // Choose Women category (name of this category should never change)
    cy.contains('Women').click()

    cy.get('#page-title-heading').should('be.visible')

    cy.url().should('include', 'women')

    cy.get(`.categories-menu > ul:first > li`)
    .its('length')
    .then((categoryLength) => {
        const categoriesNumber = parseInt((Math.random()*categoryLength)+1)
        cy.get(`.categories-menu > ul:nth-child(2) > li:nth-child(${categoriesNumber}) > a`).click()
    })

    //Choose random amount of products and put it to the basket
    const amountOfProduts = parseInt(Math.random()*4)

    for (var i=0; i<=amountOfProduts; i++) {
        cy.get('.product-item-info:visible').its('length').then((productsLength) => {
            const productsNumber = parseInt((Math.random()*productsLength))
            cy.get('.product-item-info:visible').eq(`${productsNumber}`).click()
        })
        
        cy.get('#product-options-wrapper [attribute-code="size"] div [role=option]').its('length').then((sizeLength) => {
            const sizeNumber = parseInt((Math.random()*sizeLength)+1)
            cy.get(`#product-options-wrapper [attribute-code="size"] div [role=option]:nth-child(${sizeNumber})`).click()
    })

        cy.get('#product-options-wrapper [attribute-code="color"] div [role=option]').its('length').then((colorLength) => {
            const colorNumber = parseInt((Math.random()*colorLength)+1)
            cy.get(`#product-options-wrapper [attribute-code="color"] div [role=option]:nth-child(${colorNumber})`).click()
            cy.get('#product-addtocart-button').click()
            cy.get('[data-ui-id=message-success]').should('be.visible')
            cy.go('back')
            cy.get('#tab-label-description').should('not.be.visible')
        })
    }

    cy.get('.action.viewcart').click({force:true})

    cy.wait('@CheckoutBasket')

    //Check if sum of products, cost of delivery and discounts is equal to sum calculated by Magento
    var sum = []
    var arraySum = []
    const nbsp = String.fromCharCode(160);
    cy.get('tbody tr td:nth-child(4) span .price').each((price) =>  {
        const priceText=price.text()
        let res = priceText.split('z')
        res = res[0].trim()
        res = parseFloat(res.replace(',', '.').replace(nbsp, ''));
        sum = arraySum.push(res)
    }).then( () => {
            sum = (arraySum).reduce(function (a,b) {
            return a+b;
          })  
          cy.log(sum)
    })
    

    cy.wait('@CheckoutBasket')

    cy.get('.loader > img').should('not.be.visible', {timeout:6000})

    var newsum=0
    cy.get('[data-th="Dostawa"]').then((deliveryPrice) => { 
        const deliveryText=deliveryPrice.text()
        let dprice = deliveryText.split('z')
        dprice = dprice[0].trim()
        dprice = parseFloat(dprice.replace(',', '.').replace(nbsp, ''));
        newsum = parseFloat(newsum) + sum + dprice
    })
    cy.get('body').then((body) => {
        if (body.find('[data-th="Zniżka"]').length > 0) {
            cy.get('[data-th="Zniżka"]').then((el) => {
            const saleText=el.text()
            let saleprice = saleText.split('z')
            saleprice = saleprice[0].trim()
            saleprice = parseFloat(saleprice.replace(',', '.').replace(nbsp, ''));
            newsum = newsum + saleprice
            })
        }
    })
    var finalprice = ''
    cy.get('[data-th="Podsumowanie"]').then( (finalSum) =>{
        const finalText=finalSum.text()
        finalprice = finalText.split('z')
        finalprice = finalprice[0].trim()
        finalprice = parseFloat(finalprice.replace(',', '.').replace(nbsp, ''));
    }).then(function() {
        expect(newsum).to.equal(finalprice)
    })
})
})
