describe ( "MagentoWishlistTest",() => {
    before(function(){
        cy.visit(`${Cypress.env('url')}`)
        })

it ('Can add products to wishlist', () => {

    cy.contains('Utwórz konto').click()

    const myLastName = parseInt(Math.random()*100000) + "Tester"
    const myMail = parseInt(Math.random()*1000000) + "testemail@testemail123.pl"

    //Type all the data in registration
    cy.url().should('eq', `${Cypress.env('url')}/customer/account/create/`, { timeout: 5000 })

    cy.get('#firstname').type('Pawel')

    cy.get('#lastname').type(myLastName)

    cy.get('#email_address').type(myMail)

    cy.get('#password').type('TestPassword1')

    cy.get('#password-confirmation').type('TestPassword1')

    cy.get('.action.submit.primary').click()

    cy.url().should('eq', `${Cypress.env('url')}/customer/account/`)

    cy.contains('Moje konto')

    cy.get('[data-ui-id="message-success"]').should('be.visible')

    const numberOfProduct = parseInt((Math.random()*6)+1)
    
    for (var i=0; i<=numberOfProduct; i++) {
        //Go to category and add to wishlist
        cy.get('.filter-options-content li').should('not.be.visible')
        cy.contains('Gear').click({force:true})
        cy.get('#page-title-heading').should('be.visible')
        cy.url().should('include', 'gear')

        cy.get(`.categories-menu > ul:first`).its('length').then((subcategoriesLength) => {
        const subcategoriesNumber = parseInt((Math.random()*subcategoriesLength)+1)
        cy.get(`.categories-menu > ul:nth-child(2) > li:nth-child(${subcategoriesNumber}) > a`).click()
    })

        cy.get('.column.main .product-item-info:visible ').its('length').then((productsLength) => {
            const productsNumber = parseInt((Math.random()*productsLength))
            cy.get('.column.main .product-item-info:visible ')
            .eq(`${productsNumber}`)
            .find('.action.towishlist')
            .click({force:true})
        })
    }
    cy.contains(`${numberOfProduct} produkt(ów)`)
})
    })
