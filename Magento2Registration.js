describe ( "MagentoRegistrationTest",() => {
    before(function(){
        cy.visit(`${Cypress.env('url')}`)
        })

it ('Can registrate user', () => {

    cy.get('.panel :nth-child(3) > a').click()

    const myLastName = parseInt(Math.random()*100000) + "Tester"
    const myMail = parseInt(Math.random()*1000000) + "testemail@testemail123.pl"

    //Type all the data
    cy.url().should('eq', `${Cypress.env('url')}/customer/account/create/`, { timeout: 5000 })

    cy.get('#firstname').type('Pawel')

    cy.get('#lastname').type(myLastName)

    cy.get('#email_address').type(myMail)

    cy.get('#password').type('TestPassword123')

    cy.get('#password-confirmation').type('TestPassword123')

    cy.get('.action.submit.primary').click()
    
    cy.url().should('eq', `${Cypress.env('url')}/customer/account/`)

    cy.contains('Moje konto')

    cy.get('[data-ui-id="message-success"]').should('be.visible')
    
})
})
